<?php

namespace Drupal\commerce_quantity_pricing\Plugin\views\field;

use Drupal\commerce_cart\Plugin\views\field\EditQuantity;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * Overrides a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_order_item_edit_quantity")
 */
class EditQuantityPricing extends EditQuantity {

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form['#attached'] = [
      'library' => ['commerce_cart/cart_form'],
    ];
    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($row);
      if ($this->options['allow_decimal']) {
        $form_display = commerce_get_entity_display('commerce_order_item', $order_item->bundle(), 'form');
        $quantity_component = $form_display->getComponent('quantity');
        $step = $quantity_component['settings']['step'];
        $precision = $step >= '1' ? 0 : strlen($step) - 2;
      }
      else {
        $step = 1;
        $precision = 0;
      }
      $form[$this->options['id']][$row_index] = [
        '#type' => 'number',
        '#title' => $this->t('Quantity'),
        '#title_display' => 'invisible',
        '#default_value' => round($order_item->getQuantity(), $precision),
        '#size' => 4,
        '#min' => 0,
        '#max' => 9999,
        '#step' => $step,
        '#required' => TRUE,
      ];
      if ($options = $this->hasSteps($order_item->getPurchasedEntity()->getProduct())) {
        $form[$this->options['id']][$row_index] = [
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => round($order_item->getQuantity(), $precision),
          '#required' => TRUE,
        ];
      }
    }

    $form['actions']['submit']['#update_cart'] = TRUE;
    $form['actions']['submit']['#show_update_message'] = TRUE;
    // Replace the form submit button label.
    $form['actions']['submit']['#value'] = $this->t('Update cart');
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (empty($triggering_element['#update_cart'])) {
      // Don't run when the "Remove" or "Empty cart" buttons are pressed.
      return;
    }

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    $cart = $order_storage->load($this->view->argument['order_id']->getValue());
    $quantities = $form_state->getValue($this->options['id'], []);
    $save_cart = FALSE;
    foreach ($quantities as $row_index => $quantity) {
      if (!is_numeric($quantity) || $quantity < 0) {
        // The input might be invalid if the #required or #min attributes
        // were removed by an alter hook.
        continue;
      }
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($this->view->result[$row_index]);
      if ($order_item->getQuantity() == $quantity) {
        // The quantity hasn't changed.
        continue;
      }

      if ($quantity > 0) {
        $order_item->setQuantity($quantity);
        $this->cartManager->updateOrderItem($cart, $order_item, FALSE);
      }
      else {
        // Treat quantity "0" as a request for deletion.
        $this->cartManager->removeOrderItem($cart, $order_item, FALSE);
      }
      $save_cart = TRUE;
    }

    if ($save_cart) {
      $cart->save();
      if (!empty($triggering_element['#show_update_message'])) {
        $this->messenger->addMessage($this->t('Your shopping cart has been updated.'));
      }
    }
  }

  /**
   * Generate steps for a product based on settings in Taxonomy term.
   *
   * @param \Drupal\commerce_product\Entity\Product $product
   *   Product to generate steps for.
   *
   * @return array
   *   Pricing steps for product.
   */
  private function hasSteps(Product $product) {
    foreach ($product->referencedEntities() as $referencedEntity) {
      $class = get_class($referencedEntity);
      if (strpos($class, 'Term') > -1) {
        if ($referencedEntity->hasField('field_quantity_pricing')) {
          $values = $referencedEntity->get('field_quantity_pricing')->getValue();
          return $this->generateSteps($values);
        }
      }
    }
    return [];
  }

  /**
   * Actually generate steps.
   *
   * @param array $values
   *   Extracted values.
   *
   * @return array
   *   Key/pair array, keyed by quantity.
   */
  private function generateSteps(array $values) {
    $result = [];
    foreach ($values as $value) {
      // If max is less than zero (or null), throw out that value.
      // This sometimes happen when a blank entry is left ("new") when editing.
      if ($value['max'] < 1) {
        continue;
      }

      // A long for loop statement ensures that we don't go from 1 -> 21.
      for ($i = $value['min']; $i <= $value['max']; $i > 1 ? $i += $value['step'] : $i += $value['step'] - 1) {
        $price = explode('/', $value['price']);
        $renderable = [
          '#theme' => 'quantity_pricing_format',
          '#price' => $price[0],
          '#currency' => $price[1],
          '#quantity' => $i,
        ];
        $rendered = $this->getRenderer()->render($renderable);

        $result[$i] = $rendered;
      }
    }
    return $result;
  }

}
