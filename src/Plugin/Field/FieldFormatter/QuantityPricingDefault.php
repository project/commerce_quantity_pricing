<?php

namespace Drupal\commerce_quantity_pricing\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'commerce_quantity_pricing' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_quantity_pricing_default",
 *   module = "commerce_quantity_pricing",
 *   label = @Translation("Commerce quantity pricing default"),
 *   field_types = {
 *     "quantity_pricing"
 *   }
 * )
 */
class QuantityPricingDefault extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything for now.
    return [];
  }

}
