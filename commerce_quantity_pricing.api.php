<?php

/**
 * @file
 * Examples of hooks and alters.
 */

/**
 * Alter the price that was defined for a particular product variation.
 *
 * @param array $price
 *   This array includes a simple array of the number and the currency.
 * @param mixed $context
 *   This array includes the following:
 *    - price_definition - A string used to calculate the price.
 *    - price_list - A complete array of price values defined.
 *    - quantity - An integer representing the quantity value.
 *    - varation - ProductVariation entity.
 */
function hook_commerce_quantity_price_alter(&$price, $context) {
  if ($context['variation'] && !$context['variation']->get('field_this_is_a_printed_envelope')->isEmpty()) {
    $is_a_printed_envelope = (bool) $context['variation']->get('field_this_is_a_printed_envelope')->getValue()[0]['value'];
    if ($is_a_printed_envelope) {
      $printed_envelope_price_increase = (float) \Drupal::configFactory()->get('mymodule.settings')->get('printed_envelope_price_increase');
      $price[0] += $printed_envelope_price_increase;
      // Must pass a string or things blow up.
      $price[0] = (string) $price[0];
    }
  }
}
