<?php

namespace Drupal\Tests\commerce_quantity_pricing\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\taxonomy\Entity\Term;
use Drupal\commerce_price\Price;

/**
 * Functional test for quantity-based pricing.
 *
 * @group commerce_quantity_pricing
 */
class QuantityPricingTest extends CommerceBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_quantity_pricing',
    'taxonomy',
    'system'
  ];

  protected $product;
  protected $taxonomy;
  protected $vocab;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->product = $this->createProduct();

    FieldStorageConfig::create([
      'entity_type' => 'commerce_product',
      'field_name' => 'field_category',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
      'cardinality' => 1,
    ])->save();

    FieldConfig::create([
      'entity_type' => 'commerce_product',
      'field_name' => 'field_category',
      'bundle' => 'default',
      'label' => 'Product Category',
    ])->save();
    $this->product = $this->createProduct();

    $this->vocab = $this->createVocab();
    FieldStorageConfig::create([
      'entity_type' => 'taxonomy_term',
      'field_name' => 'field_quantity_pricing',
      'type' => 'quantity_pricing',
      'cardinality' => 1,
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_quantity_pricing',
      'entity_type' => 'taxonomy_term',
      'bundle' => $this->vocab->id(),
    ])->save();
    $this->taxonomy = $this->createTaxonomy($this->vocab);

    $this->taxonomy->set('field_quantity_pricing', [
      'value' => [
        'min' => 1,
        'max' => 2,
        'step' => 1,
        'price' => '1.99/USD',
      ],
    ])->save();
    $this->product->set('field_category', $this->taxonomy->id())->save();
  }

  /**
   * Tests pricing.
   */
  public function testQuantityPricing() {
    $order_item = OrderItem::create([
      'type' => 'default',
      'unit_price' => $this->product->getDefaultVariation()->getPrice(),
      'purchased_entity' => $this->product,
      'quantity' => 1,
    ]);
    $order_item->save();
    $order = Order::create([
      'type' => 'default',
      'state' => 'completed',
      'uid' => 1,
      'order_items' => [$order_item],
      'order_number' => '1',
    ]);
    $order->recalculateTotalPrice();
    $order->save();

    $this->assertEquals(new Price('1.99', 'USD'), $order->getTotalPrice());
  }

  /**
   * Creates random product w/ variation.
   *
   * @return mixed
   *   Product entity.
   */
  private function createProduct() {
    $product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'body' => ['value' => $this->randomString()],
      'variations' => [
        $this->createEntity('commerce_product_variation', [
          'type' => 'default',
          'sku' => $this->randomMachineName(),
          'price' => [
            'number' => '10.99',
            'currency_code' => 'USD',
          ],
        ]),
      ],
    ]);
    return $product;
  }

  private function createVocab() {
    $vocabulary = Vocabulary::create([
      'vid' => 'categories',
      'name' => $this->randomString(),
    ]);
    $vocabulary->save();

    return $vocabulary;
  }

  private function createTaxonomy($vocabulary) {
    $term = Term::create([
      'vid' => $vocabulary->id(),
      'name' => $this->randomString(),
    ]);
    $term->save();

    return $term;
  }

}
